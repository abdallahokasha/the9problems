// using js ;) 
const compose = (...fns) => fns.reduce((f, g) => (...args) => f(g(...args)));

const inc = x => x+1;
const square = x => x*x;

const func = compose(
  arg => inc(arg),
  arg => square(arg)
);

console.log(func(2));
