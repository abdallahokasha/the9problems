#include <iostream>
#include <string>
#include <sstream>

using namespace std;

string intToString(int num)
{
    string s;
    stringstream ss;
    ss << num;
    ss >> s;
    return s;
}

int stringToInt(string s)
{
    int num;
    stringstream ss;
    ss << s;
    ss >> num;
    return num;
}

string runLengthEncode(string s)
{
    string ret;
    int cnt = 1, len = (int)s.size();
    for(int i = 1; i < len; ++i) {
        if(s[i] != s[i - 1]) {
            string cntStr = intToString(cnt);
            ret += s[i - 1];
            ret += cntStr;
            cnt = 0;
        }
        ++cnt;
    }
    if(cnt) {
        ret += s[len - 1];
        ret += intToString(cnt);
    }
    return ret;
}
string runLengthDecode(string s)
{
    string num, ret;
    char c = s[0];
    int len = (int)s.size();
    for(int i = 1; i < len; ++i) {
        if(isalpha(s[i])) {
            ret.insert(ret.end(), stringToInt(num), c);
            c = s[i];
            num = "";
        }
        if(isdigit(s[i]))
            num += s[i];
    }
    if(num != "")
        ret.insert(ret.end(), stringToInt(num), c);
    return ret;
}

int main()
{
    string s;
    cin >> s;
    string endcodedStr = runLengthEncode(s);
    cout << "runLengthEncode(" << s << ") : " << endcodedStr << "\n";
    cout << "runLengthDecode(" << endcodedStr << ") : " << runLengthDecode(endcodedStr) << "\n";
    return 0;
}
