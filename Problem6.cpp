//#include <bits/stdc++.h>
#include <iostream>

using namespace std;

int** transposeMatrix(int r, int c, int** matrix)
{
    int** ret = new int*[c];
    for(int i = 0; i < c; ++i) {
        ret[i] = new int[r];
        for(int j = 0; j < r; ++j) {
            ret[i][j] = matrix[j][i];
            // cout << ret[i][j] << " ";
        }
        // cout << "\n";
    }
    return ret;
}

int main()
{
    int r, c;
    cout << "Enter number of rows: ";
    cin >> r;
    cout << "Enter number of columns: ";
    cin >> c;
    cout << "Enter the maxtrix elements row by row: ";
    int** arr = new int*[r];
    for(int i = 0; i < r; ++i) {
        arr[i] = new int[c];
        for(int j = 0; j < c; ++j) {
            cin >> arr[i][j];
        }
    }
    cout << "transposed maxtrix: \n";
    int** ret = new int*[c];
    ret = transposeMatrix(r, c, arr);
    for(int i = 0; i < c; ++i, cout << "\n") {
        for(int j = 0; j < r; ++j) {
            cout << ret[i][j] << " ";
        }
    }
    return 0;
}
