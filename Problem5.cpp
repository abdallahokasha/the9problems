//#include <bits/stdc++.h>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <map>

using namespace std;

vector<string> unique(vector<string> vec)
{
    vector<string> ret;
    int len = (int)vec.size();
    map<string, int> mp;
    for(int i = 0; i < (int)vec.size(); ++i) {
        mp[vec[i]]++;
    }
    for(int i = 0; i < (int)vec.size(); ++i) {
        if(mp[vec[i]] == 1)
            ret.push_back(vec[i]);
    }
    return ret;
}

int main()
{
    int n;
    cout << "enter the number of words\n";
    cin >> n;
    vector<string> vec(n);
    for(int i = 0; i < n; ++i)
        cin >> vec[i];
    vector<string> ret = unique(vec);
    cout << "unique word(s) \n";
    for(int i = 0; i < (int)ret.size(); ++i)
        cout << ret[i] << " ";
    cout << "\n";
    return 0;
}
