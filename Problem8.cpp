//#include <bits/stdc++.h>
#include <iostream>
#include <string>
#include <vector>
#include <map>

using namespace std;

/*
 * Complexity O(N) at most, if the duplicate element is the last one.
 */
int getFirstDuplicateIndex(vector<int> vec)
{
    // we can use counter array, but it will a litte bit limit sol.
    // int cnt [1000009];
    map<int, int> mp;
    int len = (int)vec.size(); 
    for(int i = 0; i < len; ++i) {
        if(mp[vec[i]])
            return i;
        mp[vec[i]]++;
    }
    return -1;
}
int main()
{
    int n;
    cout << "Enter number of elements: ";
    cin >> n;
    vector<int> vec(n);
    for(int i = 0; i < n; ++i)
        cin >> vec[i];
    cout << "first duplicate index is: " << getFirstDuplicateIndex(vec) << "\n";
    return 0;
}
