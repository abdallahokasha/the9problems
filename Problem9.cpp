//#include <bits/stdc++.h>
#include <iostream>
#include <vector>
#include <queue>
using namespace std;

struct Node {
    int value;
    vector<Node*> childerns;
};

int getSumOfNode(Node* root)
{
    int sum = 0;
    if(root == NULL)
        return sum;

    queue<Node*> qu;
    qu.push(root);

    while(!qu.empty()) {

        int sz = qu.size();

        while(sz > 0) {
            Node* n = qu.front();
            cout << n->value << " ";

            qu.pop();
            sz--;
            sum += n->value;
            for(int i = 0; i < n->childerns.size(); ++i)
                qu.push(n->childerns[i]);
        }
        cout << "\n";
    }
    return sum;
}

int main()
{
    Node* root = new Node();
    Node* node1 = new Node();
    Node* node2 = new Node();
    Node* node3 = new Node();
    Node* node4 = new Node();
    
    root->value = 5;    
    node1->value = 10;
    node2->value = 3;
    node3->value = 4;
    node4->value = 7;

    root->childerns.push_back(node1);
    node1->childerns.push_back(node2);
    node2->childerns.push_back(node3);
    node3->childerns.push_back(node4);

    cout << getSumOfNode(root);
    return 0;
}