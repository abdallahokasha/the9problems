#include <iostream>
#include <string>
using namespace std;

bool isPalindrome1(string s)
{
    int len = (int)s.size(), mid;
    mid = len / 2;
    for(int i = 0, j = len - 1; i < mid; ++i, --j) {
        if(s[i] != s[j])
            return false;
    }
    return true;
}

bool isPalindrome2(string str)
{
    string orginalStr = str;
    reverse(str.begin(), str.end());
    return str == orginalStr;
}
int main()
{
    string s;
    cin >> s;
    cout << isPalindrome2(s) << endl;
    return 0;
}
